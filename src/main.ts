import { createApp } from 'vue';
import App from './App.vue';
import './assets/fonts.css';
import './assets/style.scss';
import "@unocss/reset/tailwind.css";


createApp(App)
    .mount('#app')
